﻿function getBill(tablesId, tablesName) {
	checkBill(tablesId).done(function (data) {
		if (data.length > 0) {
			billsId = data[0].id;
			let txt = `<div>
				<p><b>${data[0].tablesName}</b></p>
				<p>${data[0].created_by}</p>
				</div >`;
			$(".table-name").html(txt);
			let str = '';
			str += `<p><b>${addCommas(data[0].sub_total)} vnđ</b></p>
				<p>${addCommas(data[0].fee_service)} vnđ</p>`;
			$("#sub-total-money-1 .col-md-4").html(str);
			$("#total-money-1 .col-md-7").html(`<p><b>${addCommas(data[0].total_money)} vnđ</b></p>`);
			getBillDetails(tablesId);
		}
		else {
			let txt = `<div>
				<p><b>${tablesName}</b></p>
				<p>${user.name}</p>
				</div >`;
			$(".table-name").html(txt);
			let str = '';
			str += '<p><b>0 vnđ</b></p>' +
				'<p>0 vnđ</p>';
			$("#sub-total-money-1 .col-md-4").html(str);
			$("#table-bill-1").html("");
			$("#table-bill-2").html("");
			$("#total-money-1 .col-md-7").html("<p><b>0 vnđ</b></p>");

			$("#main-order-1 .checkout").removeClass("active");
			$("#main-order-1 .btn-temp-order").removeClass("active");
		}
	})
}

function createBill(bills) {
	return $.ajax({
		url: GetBill,
		method: "POST",
		dataType: "json",
		async: false,
		contentType: "application/json",
		data: JSON.stringify(bills)
	});
}

function updateBill(billsId, bills) {
	return $.ajax({
		url: GetBill + "/" + billsId,
		method: "PUT",
		dataType: "json",
		async: false,
		contentType: "application/json",
		data: JSON.stringify(bills)
	});
}

function checkBill(tablesId) {
	return $.ajax({
		url: GetBill + "/?TableId=" + tablesId,
		method: "GET",
		dataType: "json",
		async: false,
		contentType: "application/json"
	});
}