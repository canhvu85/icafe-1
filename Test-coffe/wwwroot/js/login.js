﻿$(document).ready(function () {
    getCity();

});

function getCity() {
    $.ajax({
        url: GetCity,
        method: "GET",
        dataType: "json",
        contentType: "application/json"
    }).done(function (data) {
        if (data.length > 0) {
            $.each(data, function (index, value) {
                $("#login .city .custom-options").append(
                    `<span class='custom-option' data-value='${value.id}'>${value.name}</span>`
                );
            });

            $("#login .city .custom-option").on("click", function () {
                let city_name = $(this).html();
                let city_id = $(this).data("value");
                $("#login .city .custom-select__trigger span").html(city_name);
                $("#login .city .custom-option.selected").removeClass("selected");
                this.classList.add("selected");
                $("#login .shop .custom-select__trigger span").html("Chọn tên quán");
                $("#login .shop .custom-options").html("");
                //let cityId = $(this).data('value');
                $.ajax({
                    url: GetShop + "/cities/" + city_id,
                    method: "GET",
                    dataType: "json",
                    contentType: "application/json"
                }).done(function (data) {
                    if (data.length > 0) {
                        let k = data.length;
                        let str = '<span class="custom-option selected" style="display: none;"></span>';
                        for (let i = 0; i < k; i++) {
                            str += `<span class="custom-option" onclick="changeShop(this,'${data[i].name}');" data-value="${data[i].id}">${data[i].name}</span>`;
                        }
                        $("#login .shop .custom-options").html(str);
                    }
                });
            });
        }
    });
}

$("#login").submit(function (event) {
    event.preventDefault();
    if (!$(this).valid()) return false;
    login();
});

function login() {
    let users = {
        username: $("#username").val(),
        password: $("#password").val(),
        ShopsId: $("#login .shop .selected").data("value")
    }
    $.ajax({
        url: LoginForm,
        method: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(users)
    }).done(function (data) {
        if (data.length > 0) {
            showSuccessbyAlert('Thông báo', 'Đăng nhập hệ thống thành công.')
            localStorage.setItem('user', JSON.stringify(data[0]));
            setTimeout(function () {
                window.location.replace("/cashier");
            }, 2000);
        } else
            showErrorbyAlert('Cảnh báo', 'Đăng nhập không thành công!')
    }).fail(function () {
        showErrorbyAlert('Cảnh báo', 'kiem tra mang!')
    });
}

$("#login").validate({
    rules: {
        username: {
            required: true,
            minlength: 3
        },
        password: {
            required: true
        }
    },
    messages: {
        username: {
            required: "Vui lòng nhập tài khoản",
            minlength: "Phải nhập 3 ký tự trở lên"
        },
        password: {
            required: "Vui lòng nhập mật khẩu"
        }
    }
});