﻿//$(document).ready(function () {
//	$(".statusTd").each(function () {
//		if (parseInt($(this).text()) == 0)
//			$(this).html("<span class='badge bg-danger'>Chưa thanh toán</span>");
//		else
//			$(this).html("<span class='badge bg-secondary'>Đã thanh toán</span>");
//	});
//});

let billsId;
let user = JSON.parse(localStorage.getItem('user'));

getBillsId();
getBillDetails(parseInt($("#billsList tr:first td:first").text()));


function getBillDetails(billsId) {
	$.ajax({
		url: "api/BillDetailsAPI/bills/" + billsId,
		method: "GET",
		dataType: "json",
		async: false,
		contentType: "application/json"
	}).done(function (data) {
		$("#billsDTList").html("");
		if (data.length > 0) {
			let text = '';
			for (let i = 0; i < data.length; i++) {
				text += "<tr>";
				text += "<td>" + (i + 1) + ".</td>";
				text += "<td><div>" + data[i].productsName + "</div><div><b>" + addCommas(data[i].price) + " vnđ</b></div></td>";
				text += "<td>" + data[i].quantity + "</td>";
				text += "<td>" + addCommas(data[i].total) + "</td>";
				text += "</tr>";
			}
			$("#billsDTList").html(text);
		}
	})
}

let startDate,
	endDate;

$('#reservation').daterangepicker({
	"showDropdowns": true,
	ranges: {
		'Today': [moment(), moment()],
		'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		'This Month': [moment().startOf('month'), moment().endOf('month')],
		'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	},
	"alwaysShowCalendars": true
});

$('#reservation').on('apply.daterangepicker', function (ev, picker) {
	startDate = picker.startDate.format('YYYY-MM-DD');
	endDate = picker.endDate.format('YYYY-MM-DD');
	console.log(startDate);
	console.log(endDate);
	findByDate();
});

function findByDate() {
	$("#example1").DataTable({
		"destroy": true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"ajax": {
			"url": "api/BillsAPI/shop/" + user.shopsId + "/date/" + startDate + "/" + endDate,
			"dataSrc": ""
		},
		columns: [
			{ data: 'id' },
			{ title: "giờ", data: 'time_out' },
			{ data: 'status' },
			{ data: 'sub_total' },
			{ data: 'fee_service' },
			{ data: 'total_money' },
			{ data: 'name' },
			{ data: 'created_by' }
		],
		columnDefs: [{
			targets: 1, render: function (data) {
				return moment(data).format('DD-MM-YYYY');
			}
		}],
		initComplete: function () {
			getBillsId();
		}
	});
}

function getBillsId() {
	$("#billsList tr").on("click", function () {
		billsId = parseInt($(this).find("td:eq(0)").text());
		getBillDetails(billsId);
		$("#billsList tr").removeClass("activeClk");
		$(this).addClass("activeClk");
	});
}