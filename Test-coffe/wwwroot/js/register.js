﻿$(document).ready(function () {
    getCityRegister();

});

function getCityRegister() {
    $.ajax({
        url: "api/CitiesAPI",
        method: "GET",
        dataType: "json",
        contentType: "application/json"
    }).done(function (data) {
        if (data.length > 0) {
            $.each(data, function (index, value) {
                $("#register .city .custom-options").append(
                    "<span class='custom-option' data-value='" + value.id + "'>" + value.name + "</span>"
                );
            });

            $("#register .city .custom-option").on("click", function () {
                let city_name = $(this).html();
                let city_id = $(this).data("value");
                $("#register .city .custom-select__trigger span").html(city_name);
                $("#register .city .custom-option.selected").removeClass("selected");
                this.classList.add("selected");
                $("#register .shop .custom-select__trigger span").html("Chọn tên quán");
                $("#register .shop .custom-options").html("");
                //let cityId = $(this).data('value');
                $.ajax({
                    url: "api/ShopsAPI/cities/" + city_id,
                    method: "GET",
                    dataType: "json",
                    contentType: "application/json"
                }).done(function (data) {
                    if (data.length > 0) {
                        let k = data.length;
                        let str = '<span class="custom-option selected" style="display: none;"></span>';
                        for (let i = 0; i < k; i++) {
                            str += '<span class="custom-option" onclick="changeShopRegister(this,\'' + data[i].name + '\');" data-value="' + data[i].id + '">' + data[i].name + '</span>';
                        }
                        $("#register .shop .custom-options").html(str);
                    }
                });
            });
        }
    });
}


$("#register").submit(function (event) {
    event.preventDefault();
    if (!$(this).valid()) return false;
    register();
});

function register() {
    let users = {
        username: $("#usernameRegister").val(),
        password: $("#passwordRegister").val(),
        ShopsId: $("#register .shop .selected").data("value")
    }
    $.ajax({
        url: "/Login/RegisterForm",
        method: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(users)
    }).done(function (data) {
        showSuccessbyAlertThen("Thông báo!", "Đăng ký tài khoản thành công!", register);
        function register() {
            $("#username").val(users.username);
            $("#password").val(users.password);
            $(".login-btn").click();
            $("#login .city .custom-option").each(function () {
                if ($(this).data("value") == $("#register .city .selected").data("value")) {
                    $(this).click();
                    $(document).ajaxStop(function () {
                        $("#login .shop .custom-option").each(function () {
                            if ($(this).data("value") == $("#register .shop .selected").data("value")) {
                                $(this).click();
                                $('#login .shop .custom-select-wrapper').click();
                            }
                        });
                    })

                }
            });
        }

        return false;
    }).fail(function (data) {
        showErrorbyAlert('Cảnh báo', data.responseText)
    });
}

$("#register").validate({
    rules: {
        usernameRegister: {
            required: true,
            minlength: 3
        },
        passwordRegister: {
            required: true
        }
    },
    messages: {
        usernameRegister: {
            required: "Vui lòng nhập tài khoản",
            minlength: "Phải nhập 3 ký tự trở lên"
        },
        passwordRegister: {
            required: "Vui lòng nhập mật khẩu"
        }
    }
});
