﻿function getBillDetails(tablesId) {
	getListBillDetails(tablesId).done(function (data) {
		if (data.length > 0) {
			var items = data.filter(function (rs) {
				return rs.status == 1;
			});

			var items2 = data.filter(function (rs) {
				return rs.status == 4;
			});

			var items3 = data.filter(function (rs) {
				return rs.status == 0;
			});
			let str = '';
			itemsPrinted = items.length;
			drawOrderPrinted(items, str, items2, tablesId);
			$("#table-bill-2").html("");
			if (items3.length > 0) {
				drawOrderNewWaiter(items3);
			}
		} else {
			//itemsPrinted = 0;
			$("#table-bill-1").html("");
			$("#table-bill-2").html("");
			$("#main-order-1 .checkout").removeClass("active");
			$("#main-order-1 .btn-temp-order").removeClass("active");
		}
	})
}

function getListBillDetails(tablesId) {
	return $.ajax({
		url: GetBillDetail + "/?TableId=" + tablesId,
		method: "GET",
		dataType: "json",
		async: false,
		contentType: "application/json"
	});
}

function createBillDetails(billDetails) {
	return $.ajax({
		url: GetBillDetail,
		method: "POST",
		dataType: "json",
		async: false,
		contentType: "application/json",
		data: JSON.stringify(billDetails)
	});
}

function updateBillDetail(billDetailsId, billDetails) {
	return $.ajax({
		url: GetBillDetail + "/" + billDetailsId,
		method: "PUT",
		dataType: "json",
		async: false,
		contentType: "application/json",
		data: JSON.stringify(billDetails)
	});
}

function billDetailsObj(id, quantity, total, status, username) {
	return {
		"id": id,
		"quantity": quantity,
		"total": total,
		"status": status,
		"updated_by": username
	}
}

function getGroupOrderPrinted(tableId) {
	return $.ajax({
		url: GetBillDetail + "/TableId/" + tableId,
		method: "GET",
		dataType: "json",
		async: false,
		contentType: "application/json"
	});
}

function getOrderPrinted(tableId) {
	return $.ajax({
		url: GetBillDetail + "/printed/" + tableId,
		method: "GET",
		dataType: "json",
		async: false,
		contentType: "application/json"
	});
}

function drawOrderPrinted(items, str, items2, tablesId) {
	if (itemsPrinted > 0) {
		for (let i = 0; i < items.length; i++) {
			let tb = new Tables(items[i].id, billsId, tablesId, items[i].productsId,
				items[i].productsName, items[i].price, items[i].quantity,
				items[i].total, items[i].status);
			tables.push(tb);
		}
		getGroupOrderPrinted(tablesId).done(function (data) {
			if (data.length > 0) {
				$.each(data, function (index, value) {
					str += drawHtml(value);
				});
				drawOrderNew(items2, str);
			}
		});
	} else
		return drawOrderNew(items2, str);
}

function drawOrderNew(items2, str) {
	for (let i = 0; i < items2.length; i++) {
		let tb = new Tables(items2[i].id, billsId, tablesId, items2[i].productsId,
			items2[i].productsName, items2[i].price, items2[i].quantity,
			items2[i].total, items2[i].status);
		tables.push(tb);

		str += drawHtml(items2[i], "temp-order");
	}

	$("#table-bill-1").html(str);
	if (items2.length > 0) {
		$("#main-order-1 .btn-temp-order").addClass("active");
		$("#main-order-1 .checkout").removeClass("active");
	} else {
		$("#main-order-1 .checkout").addClass("active");
		$("#main-order-1 .btn-temp-order").removeClass("active");
	}
}

function drawHtml(value, class_css) {
	return `<div class="bill-items ${class_css}">
		<div class="col-md-5">
		<p>${value.productsName}</p>
		<p>Giá: ${addCommas(value.price)} vnđ</p>
		</div>
		<div class="col-md-3" style="text-align: center;">
		<button class="btn-minus"><i class="fa fa-minus"></i></button>
		<span>${value.quantity}</span>
		<button class="btn-plus"><i class="fa fa-plus"></i></button>
		</div>
		<div class="col-md-4" style="text-align: right;">
		<p>${addCommas(value.total)} vnđ</p>
		</div>
		</div>`;
}

function getOrderNewWaiter(tablesId) {
	$.ajax({
		//url: GetBillDetail + "/new/" + tablesId,
		method: "GET",
		dataType: "json",
		async: false,
		contentType: "application/json"
	}).done(function (data) {
		if (data.length > 0) {
			drawOrderNewWaiter(data);
		}
	});
}

function drawOrderNewWaiter(data) {
	let str = '';
	$.each(data, function (index, value) {
		str += drawHtml(value, "temp-order-waiter");
	});
	$("#table-bill-2").html(str);
	$("#main-order-1 .btn-temp-order").addClass("active");
	$("#main-order-1 .checkout").removeClass("active");
}