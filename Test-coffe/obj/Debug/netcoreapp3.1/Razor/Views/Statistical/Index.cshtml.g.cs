#pragma checksum "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d6b374a310fcec8d0ce8a3561d2d4fce2b88bd49"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Statistical_Index), @"mvc.1.0.view", @"/Views/Statistical/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\_ViewImports.cshtml"
using Test_coffe;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\_ViewImports.cshtml"
using Test_coffe.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d6b374a310fcec8d0ce8a3561d2d4fce2b88bd49", @"/Views/Statistical/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8a9c4b61d8b79881e2f0daff4b36bd1677cc5421", @"/Views/_ViewImports.cshtml")]
    public class Views_Statistical_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Test_coffe.Models.Bills>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/AdminLTE-3.0.2/plugins/daterangepicker/daterangepicker.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/style.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/AdminLTE-3.0.2/plugins/moment/moment.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/AdminLTE-3.0.2/plugins/daterangepicker/daterangepicker.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/script.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/statistical.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
  
    ViewData["Title"] = "Index";
   

#line default
#line hidden
#nullable disable
            WriteLiteral("\n");
            WriteLiteral("\n");
            DefineSection("css", async() => {
                WriteLiteral("\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "d6b374a310fcec8d0ce8a3561d2d4fce2b88bd496093", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "d6b374a310fcec8d0ce8a3561d2d4fce2b88bd497270", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\n");
            }
            );
            WriteLiteral(@"
<!-- Content Header (Page header) -->
<section class=""content-header"">
    <div class=""container-fluid"">
        <div class=""row mb-2"">
            <div class=""col-sm-6"">
                <h1>DataTables</h1>
            </div>
            <div class=""col-sm-6"">
                <ol class=""breadcrumb float-sm-right"">
                    <li class=""breadcrumb-item""><a href=""#"">Home</a></li>
                    <li class=""breadcrumb-item active"">DataTables</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class=""content"">
    <div class=""row"">
        <div class=""col-6"">
            <div class=""card card-primary"">
                <div class=""card-header"">
                    <h3 class=""card-title"">Date picker</h3>
                </div>
                <div class=""card-body"">
                    <!-- Date range -->
                    <div class=""form-group"">
                        <div class=""row"">
                       ");
            WriteLiteral(@"     <div class=""col-3"">
                                <label>Date range:</label>
                            </div>

                            <div class=""col-9"">
                                <div class=""input-group"">
                                    <div class=""input-group-prepend"">
                                        <span class=""input-group-text"">
                                            <i class=""far fa-calendar-alt""></i>
                                        </span>
                                    </div>
                                    <input type=""text"" class=""form-control float-right"" id=""reservation"">
                                </div>
                            </div>
                        </div>
                        <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
    <div class=""row"">
        <div class=""col-8"">
   ");
            WriteLiteral(@"         <div class=""card"">
                <div class=""card-header"">
                    <h3 class=""card-title"">DataTable with default features</h3>
                </div>
                <!-- /.card-header -->
                <div class=""card-body table-responsive"">
                    <table id=""example1"" class=""table table-bordered table-striped"">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>
                                    ");
#nullable restore
#line 90 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                               Write(Html.DisplayNameFor(model => model.time_out));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                </th>\n                                <th>\n                                    ");
#nullable restore
#line 93 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                               Write(Html.DisplayNameFor(model => model.status));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                </th>\n                                <th>\n                                    ");
#nullable restore
#line 96 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                               Write(Html.DisplayNameFor(model => model.sub_total));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                </th>\n                                <th>\n                                    ");
#nullable restore
#line 99 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                               Write(Html.DisplayNameFor(model => model.fee_service));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                </th>\n                                <th>\n                                    ");
#nullable restore
#line 102 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                               Write(Html.DisplayNameFor(model => model.total_money));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                </th>\n                                <th>\n                                    ");
#nullable restore
#line 105 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                               Write(Html.DisplayNameFor(model => model.Tables));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                </th>\n                                <th>user</th>\n                            </tr>\n                        </thead>\n                        <tbody id=\"billsList\">\n\n\n\n");
#nullable restore
#line 114 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                             foreach (var item in Model)
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <tr>\n                                    <td>\n                                        ");
#nullable restore
#line 118 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                                   Write(Html.DisplayFor(modelItem => item.id));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                    </td>\n                                    <td>\n                                        ");
#nullable restore
#line 121 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                                   Write(Html.ValueFor(modelItem => item.time_out, "{0:dd/MM/yyyy}"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n");
            WriteLiteral("                                    </td>\n                                    <td id=\"statusTd\">\n");
#nullable restore
#line 125 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                                           var txt = "";
                                            if (item.status == 0)
                                                txt = "<span class='badge bg-danger'>Chưa thanh toán</span>";
                                            else
                                                txt = "<span class='badge bg-secondary'>Đã thanh toán</span>";
                                        

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        ");
#nullable restore
#line 131 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                                   Write(Html.Raw(txt));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n\n\n");
            WriteLiteral("                                    </td>\n                                    <td>\n                                        ");
#nullable restore
#line 137 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                                   Write(Html.DisplayFor(modelItem => item.sub_total));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                    </td>\n                                    <td>\n                                        ");
#nullable restore
#line 140 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                                   Write(Html.DisplayFor(modelItem => item.fee_service));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                    </td>\n                                    <td>\n\n                                        ");
#nullable restore
#line 144 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                                   Write(addCommas(item.total_money.ToString()));

#line default
#line hidden
#nullable disable
            WriteLiteral(" vnđ\n\n\n\n");
            WriteLiteral("                                    </td>\n                                    <td>\n                                        ");
#nullable restore
#line 151 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                                   Write(Html.DisplayFor(modelItem => item.Tables.name));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                    </td>\n                                    <td>\n                                        ");
#nullable restore
#line 154 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                                   Write(Html.DisplayFor(modelItem => item.created_by));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                    </td>\n                                </tr>\n");
#nullable restore
#line 157 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
                            }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

        <div class=""col-4"">
            <div class=""card"">
                <div class=""card-header"">
                    <h3 class=""card-title"">Bordered Table</h3>
                </div>
                <!-- /.card-header -->
                <div class=""card-body table-responsive"" style=""max-height: 101vh"">
                    <table class=""table table-bordered"" style=""border: 1px solid #dee2e6"">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ten / gia</th>
                                <th>sl</th>
                                <th>tien</th>
                            </tr>
                        </thead>
                        <tbody id=""billsDTList"">
                        </tbody>
                    </table>
                </div>
                <");
            WriteLiteral("!-- /.card-body -->\n            </div>\n        </div>\n        <!-- /.col -->\n    </div>\n    <!-- /.row -->\n</section>\n<!-- /.content -->\n\n");
            DefineSection("scripts", async() => {
                WriteLiteral(@"
    <script type=""text/javascript"">
        function addCommas(nStr) {
            nStr += '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(nStr)) {
                nStr = nStr.replace(rgx, '$1' + '.' + '$2');
            }
            return nStr;
    </script>
    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d6b374a310fcec8d0ce8a3561d2d4fce2b88bd4920049", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d6b374a310fcec8d0ce8a3561d2d4fce2b88bd4921147", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d6b374a310fcec8d0ce8a3561d2d4fce2b88bd4922245", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d6b374a310fcec8d0ce8a3561d2d4fce2b88bd4923343", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\n");
            }
            );
        }
        #pragma warning restore 1998
#nullable restore
#line 7 "C:\Users\Phan Anh Vu\Downloads\icafe-vu\icafe-vu\Test-coffe\Views\Statistical\Index.cshtml"
           
    public string addCommas(string nStr)
    {
        nStr = new String(nStr.ToCharArray().Reverse().ToArray());
        var groups = nStr.Select((c, ix) => new { Char = c, Index = ix })
                     .GroupBy(x => x.Index / 3)
                     .Select(g => String.Concat(g.Select(x => x.Char)));
        string result = string.Join(".", groups);
        result = new String(result.ToCharArray().Reverse().ToArray());
        return result;
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Test_coffe.Models.Bills>> Html { get; private set; }
    }
}
#pragma warning restore 1591
