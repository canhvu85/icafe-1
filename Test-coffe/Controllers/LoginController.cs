﻿using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MvcHaack.Ajax;
using Test_coffe.Models;

namespace Test_coffe.Controllers
{
    public class LoginController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LoginController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            //Console.WriteLine("====        " + HttpContext.Session.GetString("emailFB"));
            //Console.WriteLine("====        " + HttpContext.Session.GetString("nameFB"));
            //Console.WriteLine("====        " + HttpContext.Session.GetString("emailGoogle"));
            //Console.WriteLine("====        " + HttpContext.Session.GetString("nameGoogle"));
            //Console.WriteLine("====        " + HttpContext.Session.GetString("username"));
            //Console.WriteLine("====        " + HttpContext.Session.GetString("ShopId"));
            return View();
        }

        // POST: Cities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([Bind("username,password")] Users users)
        {

            if (ModelState.IsValid)
            {
                if (UserExists2(users.username, users.password))
                {
                    HttpContext.Session.SetString("username", users.username);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Đăng Nhập Không Đúng");
                    TempData["Message"] = "Invalid";
                    return RedirectToAction(nameof(Index));
                }
            }
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists2(string username, string password)
        {
            return _context.Users.Any(e => e.username == username && e.password == password);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        public JsonResult FacebookLogin(string name, string email)
        {
            Console.WriteLine(email);
            Console.WriteLine(name);

            HttpContext.Session.SetString("emailFB", email);
            HttpContext.Session.SetString("nameFB", name);


            return Json(new { success = "True" });

        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        public JsonResult FacebookLogOut()
        {

            HttpContext.Session.Clear();
            Console.WriteLine("logout====        " + HttpContext.Session.GetString("emailFB"));
            Console.WriteLine("logout====        " + HttpContext.Session.GetString("nameFB"));
            return Json(new { success = "True" });

        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        public JsonResult GoogleLogin(string name, string email)
        {
            Console.WriteLine(email);
            Console.WriteLine(name);

            HttpContext.Session.SetString("emailGoogle", email);
            HttpContext.Session.SetString("nameGoogle", name);

            return Json(new { success = "True" });

        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        public JsonResult GoogleLogOut()
        {

            HttpContext.Session.Clear();
            Console.WriteLine("logout====        " + HttpContext.Session.GetString("emailGoogle"));
            Console.WriteLine("logout====        " + HttpContext.Session.GetString("nameGoogle"));
            return Json(new { success = "True" });

        }

        public async Task<IActionResult> Test()
        {
            Console.WriteLine("====        " + HttpContext.Session.GetString("emailFB"));
            Console.WriteLine("====        " + HttpContext.Session.GetString("nameFB"));
            return View();
        }

        [HttpPost]
        public IActionResult LoginForm([FromBody] Users users)
        {
            var dateNow = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            var result = from u in _context.Users
                          where u.username == users.username &&
                          u.password == users.password &&
                          u.ShopsId == users.ShopsId &&
                          u.Shops.time_open <= dateNow &&
                          u.Shops.time_close >= dateNow &&
                          u.isDeleted == false
                          select new
                          {
                              u.id,
                              u.name,
                              u.images,
                              u.username,
                              u.password,
                             u.permalink,
                              positionsId = u.PositionsId,
                              shopsId = u.ShopsId
                          };
            //HttpContext.Session.SetString("username", users.username);
            HttpContext.Session.SetObjectAsJson("user", users);
            return Ok(result);
        }

        //[HttpPost]
        //public async Task<IActionResult> RegisterForm([FromBody] Users users)
        //{
        //    Console.WriteLine(users.username);
        //    users.PositionsId = 6;
        //    _context.Add(users);
        //    await _context.SaveChangesAsync();
        //    //return RedirectToAction("Index", "Login");
        //    return RedirectToAction(nameof(Index));
        //}

        [HttpPost]
        public async Task<ActionResult<Users>> RegisterForm([FromBody] Users users)
        {
            if (ModelState.IsValid)
            {
                if (!_context.Users.Any(u => u.username == users.username))
                {
                    users.PositionsId = 1;
                    _context.Users.Add(users);
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetUser", new { id = users.id }, users);
                }
                else
                {
                    return Content("Tên này đã tồn tại trong hệ thống !");
                }
            }
            else
            {
                //ViewBag.Message = "Vui lòng kiểm tra lại thông tin";
                //return BadRequest();
                return Content("Vui lòng kiểm tra lại thông tin");
            }
        }
    }
}
